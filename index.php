<?php

  $css = './assets/css/estilos.min.css';
  $js  = './assets/js/funciones.min.js';

?>

<?php

  $update = false;

  if( isset($_GET['actualizar']) ) {
    $hash_get = hash('sha256', $_GET['actualizar']);
    $hash_correcto = 'eeb36e726e3ffec16da7798415bb4e531bf8a57fbe276fcc3fc6ea986cb02e9a';
    if ( $hash_get === $hash_correcto ) {
      $enlace = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQP7GtwfwL7ZAP5scvA1noXRCqOklyv83B3KVJkepv4L1OH8hT3LdH8ZTCaShNJ3vgn3VYTdd2GB5WT/pub?gid=867799042&single=true&output=csv';
      $handle = fopen($enlace, 'r');
      if ( !$handle ) {
        $update = 'error_abrir_drive';
      } else {
        $csv = fgetcsv($handle);
        if ( !$csv ) {
          $update = 'error_leer_csv';
        } else {
          $json = $csv[0];
          $fp = fopen('./tabla.js', 'w+');
          if ( !$fp ) {
            $update = 'error_abrir_archivo';
          } else {
            $fout = fwrite($fp, "datos=".$json);
            fclose($fp);
            if ( !$fp ) {
              $update = 'error_escritura';
            } else {
              $update = true;
            }
          }
        }
      }
      $update = true;
    } else {
      $update = 'error_hash';
    }
  }

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <link rel="preconnect" href="https://cdnjs.cloudflare.com" crossorigin>

    <!-- Título de la página -->
    <title>
      Lista de grados de matemáticas
    </title>

    <!-- Hojas de estilo -->
    
    <!--   - Tipografías e iconos -->
    
    <link rel="preload" 
          href="./assets/fonts/subset-Inter-UI-Regular.woff2" 
          as="font">
    <link rel="preload" 
          href="./assets/fonts/subset-Inter-UI-SemiBold.woff2" 
          as="font">

    <link rel="stylesheet" 
          type="text/css" 
          href=<?php echo $css;?>>

    <link rel="stylesheet" 
          type="text/css" 
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css" 
          integrity="sha256-nAmazAk6vS34Xqo0BSrTb+abbtFlgsFK7NKSi6o7Y78=" 
          crossorigin="anonymous" />

    <!-- Favicons, cortesía de faviconit.com -->
    <link rel="shortcut icon"                         href="./assets/img/favicons/logo-anem.ico">
    <link rel="icon" sizes="16x16 32x32 64x64"        href="./assets/img/favicons/logo-anem.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="./assets/img/favicons/logo-anem-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="./assets/img/favicons/logo-anem-160.png">
    <link rel="icon" type="image/png" sizes="96x96"   href="./assets/img/favicons/logo-anem-96.png">
    <link rel="icon" type="image/png" sizes="64x64"   href="./assets/img/favicons/logo-anem-64.png">
    <link rel="icon" type="image/png" sizes="32x32"   href="./assets/img/favicons/logo-anem-32.png">
    <link rel="icon" type="image/png" sizes="16x16"   href="./assets/img/favicons/logo-anem-16.png">
    <link rel="apple-touch-icon"                      href="./assets/img/favicons/logo-anem-57.png">
    <link rel="apple-touch-icon" sizes="114x114"      href="./assets/img/favicons/logo-anem-114.png">
    <link rel="apple-touch-icon" sizes="72x72"        href="./assets/img/favicons/logo-anem-72.png">
    <link rel="apple-touch-icon" sizes="144x144"      href="./assets/img/favicons/logo-anem-144.png">
    <link rel="apple-touch-icon" sizes="60x60"        href="./assets/img/favicons/logo-anem-60.png">
    <link rel="apple-touch-icon" sizes="120x120"      href="./assets/img/favicons/logo-anem-120.png">
    <link rel="apple-touch-icon" sizes="76x76"        href="./assets/img/favicons/logo-anem-76.png">
    <link rel="apple-touch-icon" sizes="152x152"      href="./assets/img/favicons/logo-anem-152.png">
    <link rel="apple-touch-icon" sizes="180x180"      href="./assets/img/favicons/logo-anem-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="./assets/img/favicons/logo-anem-144.png">
    <meta name="msapplication-config"    content="./assets/img/favicons/browserconfig.xml">

    <!-- Tags de SEO para Google -->
    <meta name="viewport"            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="description"         content="Listado completo de todos los grados de matemáticas y estadística que puedes estudiar en universidades españolas." />
    <meta name="google"              content="nositelinkssearchbox" />

    <!-- Twitter Card -->
    <meta name="twitter:card"        content="summary_large_image">
    <meta name="twitter:site"        content="@ANEM_mat">
    <meta name="twitter:title"       content="Lista de grados de matemáticas">
    <meta name="twitter:description" content="Listado completo de todos los grados de matemáticas y estadística que puedes estudiar en universidades españolas.">
    <meta name="twitter:creator"     content="@ANEM_mat">
    <meta name="twitter:image"       content="https://www.anemat.com/guia/img/imagen_seo_xl.png">

    <!-- Open Graph (para Facebook) -->
    <meta property="og:title"        content="Lista de grados de matemáticas" />
    <meta property="og:type"         content="article" />
    <meta property="og:url"          content="https://www.anemat.com/grados/" />
    <meta property="og:image"        content="img/imagen_seo_xl.png" />
    <meta property="og:image:width"  content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:description"  content="Listado completo de todos los grados de matemáticas y estadística que puedes estudiar en universidades españolas." /> 
    <meta property="og:site_name"    content="Asociación Nacional de Estudiantes de Matemáticas" />

</head>

<body class="index-page has-navbar-fixed-top">

  <nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
    <div class="container">

      <div class="navbar-brand">
        <a class="navbar-item" href="https://www.anemat.com">
          <img src="./assets/img/logo.min.png" width="48" height="48" alt="Logo de la ANEM">
        </a>
        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navegacion">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navegacion" class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item">
            Listado
          </a>
          <a class="navbar-item" href="./mapa/">
            Mapa
          </a>
          <a class="navbar-item" href="./informacion/">
            Información
          </a>
        </div>

        <div class="navbar-end">
          <div class="navbar-item">
            Listado de grados de matemáticas
          </div>
        </div>
      </div>

    </div>
  </nav>

  <!-- Contenido principal de la página -->
  <div class="main section" role="main">

    <div class="contenedor-titulo">
      <div class="container">
        <h1 class="title">
          Listado completo
        </h1>
        <h2 class="subtitle">
          de grados de matemáticas
        </h2>
      </div>
    </div>

    <div class="container">

      <div class="content">
        <div class="columns">
          <div class="column">
            <div class="field">
              <label class="label" for="select-uni">Universidad</label>
              <div id="select-uni-parent" class="control">
                <input  id="select-fake-uni" class="input" type="text">
                <select id="select-uni" class="multselect" multiple="multiple" data-toggle="select" style="display: none;">
                  <option value="UA"     >Universidad de Alicante</option>
                  <option value="UAL"    >Universidad de Almería</option>
                  <option value="UAB"    >Universitat Autònoma de Barcelona</option>
                  <option value="UAM"    >Universidad Autónoma de Madrid</option>
                  <option value="UB"     >Universitat de Barcelona</option>
                  <option value="UC"     >Universidad de Cantabria</option>
                  <option value="UCA"    >Universidad de Cádiz</option>
                  <option value="UC3M"   >Universidad Carlos III de Madrid</option>
                  <option value="UCM"    >Universidad Complutense de Madrid</option>
                  <option value="UEX"    >Universidad de Extremadura</option>
                  <option value="UGR"    >Universidad de Granada</option>
                  <option value="UIB"    >Universitat de les Illes Balears</option>
                  <option value="ULL"    >Universidad de La Laguna</option>
                  <option value="UMA"    >Universidad de Málaga</option>
                  <option value="UM"     >Universidad de Murcia</option>
                  <option value="UNIOVI" >Universidad de Oviedo</option>
                  <option value="UPV"    >Universidad del País Vasco - Euskal Herriko Unibersitatea</option>
                  <option value="UPC"    >Universitat Politècnica de Catalunya</option>
                  <option value="UPM"    >Universidad Politécnica de Madrid</option>
                  <option value="URJC"   >Universidad Rey Juan Carlos</option>
                  <option value="USAL"   >Universidad de Salamanca</option>
                  <option value="USC"    >Universidade de Santiago de Compostela</option>
                  <option value="US"     >Universidad de Sevilla</option>
                  <option value="UNED"   >Universidad Nacional de Educación a Distancia</option>
                  <option value="UV"     >Universitat de València</option>
                  <option value="UVA"    >Universidad de Valladolid</option>
                  <option value="UNIZAR" >Universidad de Zaragoza</option>
                </select>
              </div>
              <p class="help">Deja el campo vacío para seleccionar todas</p>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <label class="label" for="select-cat">Categoría</label>
              <div id="select-cat-parent" class="control">
                <input  id="select-fake-cat" class="input" type="text">
                <select id="select-cat" class="multselect" multiple="multiple" data-toggle="select" style="display: none;">
                  <option value="est">Estadística</option>
                  <option value="dat">Ciencia de datos</option>
                  <option value="com">Computación</option>
                  <option value="mat">Matemáticas fundamentales</option>
                  <option value="fis">Física</option>
                  <option value="soc">Sociología</option>
                  <option value="inf">Informática</option>
                  <option value="edu">Educación</option>
                  <option value="apl">Matemáticas aplicadas</option>
                  <option value="eco">Economía</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="columns">
          <div class="column">
            <div class="field">
              <label class="label" for="select-prov">Provincia</label>
              <div id="select-prov-parent" class="control">
                <input  id="select-fake-prov" class="input" type="text">
                <select id="select-prov" class="multselect" multiple="multiple" data-toggle="select" style="display: none;">
                  <option value="C">A Coruña</option>
                  <option value="VI">Araba/Álava</option>
                  <option value="AB">Albacete</option>
                  <option value="A">Alicante/Alacant</option>
                  <option value="AL">Almería</option>
                  <option value="O">Asturias</option>
                  <option value="AV">Ávila</option>
                  <option value="BA">Badajoz</option>
                  <option value="PM">Balears</option>
                  <option value="B">Barcelona</option>
                  <option value="BI">Bizkaia</option>
                  <option value="BU">Burgos</option>
                  <option value="CC">Cáceres</option>
                  <option value="CA">Cádiz</option>
                  <option value="S">Cantabria</option>
                  <option value="CS">Castellón/Castelló​</option>
                  <option value="CR">Ciudad Real</option>
                  <option value="CO">Córdoba</option>
                  <option value="CU">Cuenca</option>
                  <option value="SS">Gipuzkoa​</option>
                  <option value="GI">Girona</option>
                  <option value="GR">Granada</option>
                  <option value="GU">Guadalajara</option>
                  <option value="H">Huelva</option>
                  <option value="HU">Huesca</option>
                  <option value="J">Jaén</option>
                  <option value="LO">La Rioja</option>
                  <option value="GC">Las Palmas</option>
                  <option value="LE">León</option>
                  <option value="L">Lleida</option>
                  <option value="LU">Lugo</option>
                  <option value="M">Madrid</option>
                  <option value="MA">Málaga</option>
                  <option value="MU">Murcia</option>
                  <option value="NA">Navarra/Nafarroa​</option>
                  <option value="OR">Ourense</option>
                  <option value="P">Palencia</option>
                  <option value="PO">Pontevedra</option>
                  <option value="SA">Salamanca</option>
                  <option value="TF">Santa Cruz de Tenerife</option>
                  <option value="SG">Segovia</option>
                  <option value="SE">Sevilla</option>
                  <option value="SO">Soria</option>
                  <option value="T">Tarragona</option>
                  <option value="TE">Teruel</option>
                  <option value="TO">Toledo</option>
                  <option value="V">Valencia/València</option>
                  <option value="VA">Valladolid</option>
                  <option value="ZA">Zamora</option>
                  <option value="Z">Zaragoza</option>
                </select>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <label class="label" for="select-ccaa">Comunidad Autónoma</label>
              <div id="select-ccaa-parent" class="control">
                <input  id="select-fake-ccaa" class="input" type="text">
                <select id="select-ccaa" class="multselect" multiple="multiple" data-toggle="select" style="display: none;">
                  <option value="AN">Andalucía</option>
                  <option value="AR">Aragón</option>
                  <option value="AS">Principado de Asturias</option>
                  <option value="CN">Canarias</option>
                  <option value="CB">Cantabria</option>
                  <option value="CM">Castilla-La Mancha</option>
                  <option value="CL">Castilla y León</option>
                  <option value="CT">Catalunya</option>
                  <option value="EX">Extremadura</option>
                  <option value="GA">Galicia</option>
                  <option value="IB">Illes Balears</option>
                  <option value="RI">La Rioja</option>
                  <option value="MD">Comunidad de Madrid</option>
                  <option value="MC">Región de Murcia</option>
                  <option value="NC">Comunidad Foral de Navarra/Nafarroako Foru Komunitatea</option>
                  <option value="PV">País Vasco/Euskal Herria</option>
                  <option value="VC">Comunidad Valenciana/Comunitat Valenciana</option>
                </select>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="buttons is-pulled-right is-marginless">
        
        <button class="button is-primary" onclick="buscar()">
          <span class="icon">
            <i class="fas fa-search"></i>
          </span>
          <span>Buscar</span>
        </button>
        
        <button class="button is-light" onclick="favoritos_vertablacompleta()">
          <span class="icon">
            <i class="fas fa-eye"></i>
          </span>
          <span>Ver tabla completa</span>
        </button>
        
        <a id="boton-imprimir" class="button is-light sin-sombra">
          <span class="icon">
            <i class="fas fa-print"></i>
          </span>
          <span>Imprimir</span>
        </a>

        <div class="dropdown is-hoverable">
          <div class="dropdown-trigger">
            <button class="button is-light" aria-haspopup="true" aria-controls="dropdown-menu">
              <span class="icon is-small">
                <i class="fas fa-star" aria-hidden="true"></i>
              </span>
              <span>Favoritos</span>
              <span class="icon is-small">
                <i class="fas fa-angle-down" aria-hidden="true"></i>
              </span>
            </button>
          </div>
          <div class="dropdown-menu" id="dropdown-menu" role="menu">
            <div class="dropdown-content">
              <a class="dropdown-item" onclick="favoritos_verenlista()">
                <span class="icon is-small">
                  <i class="fas fa-eye" aria-hidden="true"></i>
                </span>
                Ver todos
              </a>
              <a class="dropdown-item" onclick="favoritos_limpiar()">
                <span class="icon is-small">
                  <i class="fas fa-trash-alt" aria-hidden="true"></i>
                </span>
                Eliminar
              </a>
            </div>
          </div>
        </div>

      </div>
      
      <br>

      <div id="grados-wrap">
        <table id="grados" class="table" width="100%">
        </table>
      </div>
    </div>


  </div>
  <!-- Fin del contenido principal de la página -->

  <!-- Importación de archivos de JavaScript -->

  <!-- jQuery -->
  <script type="text/javascript" 
          src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" 
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" 
          crossorigin="anonymous"></script>

  <!-- DataTables -->
  <script type="text/javascript" 
          src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js" 
          integrity="sha256-t5ZQTZsbQi8NxszC10CseKjJ5QeMw5NINtOXQrESGSU=" 
          crossorigin="anonymous"></script>

  <!-- Chart.js -->
  <script type="text/javascript" 
          src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js" 
          integrity="sha256-oSgtFCCmHWRPQ/JmR4OoZ3Xke1Pw4v50uh6pLcu+fIc=" 
          crossorigin="anonymous"></script>

  <script type="text/javascript" 
          src="tabla.js"></script>

  <script type="text/javascript" 
          src=<?php echo $js;?>></script>

  <script type="text/javascript">
    function buscaclics() {
      
    };

    $(document).ready(function(){
      table = crear_tabla();
      mounted("uni");
      mounted("cat");
      mounted("prov");
      mounted("ccaa");
      $("#boton-imprimir").click(function(){
        window.open("./imprimir/" + window.location.search, "_blank");
      })
    });
  </script>

  <?php if ( $update === true ): ?>
    <script type="text/javascript">console.log('Datos actualizados con éxito.');</script>
  <?php elseif ( $update === 'error_hash' ): ?>
    <script type="text/javascript">console.log('Contraseña de actualización incorrecta.');</script>
  <?php elseif ( $update === 'error_abrir_drive' ): ?>
    <script type="text/javascript">console.log('Error al intentar abrir el documento de Drive.');</script>
  <?php elseif ( $update === 'error_leer_csv' ): ?>
    <script type="text/javascript">console.log('Error de lectura del documento CSV.');</script>
  <?php elseif ( $update === 'error_abrir_archivo' ): ?>
    <script type="text/javascript">console.log('Error al abrir el archivo local.');</script>
  <?php elseif ( $update === 'error_escritura' ): ?>
    <script type="text/javascript">console.log('Error al guardar los datos nuevos.');</script>
  <?php endif ?> 

</body>

</html>