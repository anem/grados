// Navbar para bulma
document.addEventListener('DOMContentLoaded', () => {
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {
        const target = el.dataset.target;
        const $target = document.getElementById(target);
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  };
});

const l_universidades = {
  "UA"       : { 
    'web'    : 'https://www.ua.es/',
    'nombre' : 'Universidad de Alicante',
  },
  "UAL"      : { 
    'web'    : 'https://www.ual.es/',
    'nombre' : 'Universidad de Almería',
  },
  "UAB"      : { 
    'web'    : 'https://www.uab.cat/',
    'nombre' : 'Universitat Autònoma de Barcelona',
  },
  "UAM"      : { 
    'web'    : 'http://uam.es',
    'nombre' : 'Universidad Autónoma de Madrid',
  },
  "UB"       : { 
    'web'    : 'https://www.ub.edu',
    'nombre' : 'Universitat de Barcelona',
  },
  "UC"       : { 
    'web'    : 'https://web.unican.es/',
    'nombre' : 'Universidad de Cantabria',
  },
  "UCA"      : { 
    'web'    : 'https://www.uca.es/',
    'nombre' : 'Universidad de Cádiz',
  },
  "UC3M"     : { 
    'web'    : 'https://www.uc3m.es/Home',
    'nombre' : 'Universidad Carlos III de Madrid',
  },
  "UCM"      : { 
    'web'    : 'https://ucm.es/',
    'nombre' : 'Universidad Complutense de Madrid',
  },
  "UEX"      : { 
    'web'    : 'https://www.unex.es/',
    'nombre' : 'Universidad de Extremadura',
  },
  "UGR"      : { 
    'web'    : 'https://www.ugr.es/',
    'nombre' : 'Universidad de Granada',
  },
  "UIB"      : { 
    'web'    : 'https://www.uib.es/',
    'nombre' : 'Universitat de les Illes Balears',
  },
  "ULL"      : { 
    'web'    : 'https://www.ull.es/',
    'nombre' : 'Universidad de La Laguna',
  },
  "UMA"      : { 
    'web'    : 'http://www.uma.es/',
    'nombre' : 'Universidad de Málaga',
  },
  "UM"       : { 
    'web'    : 'https://www.um.es/',
    'nombre' : 'Universidad de Murcia',
  },
  "UNIOVI"   : { 
    'web'    : 'https://www.uniovi.es/',
    'nombre' : 'Universidad de Oviedo',
  },
  "EHU"      : { 
    'web'    : 'https://www.ehu.eus',
    'nombre' : 'Universidad del País Vasco - Euskal Herriko Unibersitatea',
  },
  "UPC"      : { 
    'web'    : 'https://www.upc.edu/ca',
    'nombre' : 'Universitat Politècnica de Catalunya',
  },
  "UPM"      : { 
    'web'    : 'http://www.upm.es/',
    'nombre' : 'Universidad Politécnica de Madrid',
  },
  "URJC"     : { 
    'web'    : 'https://urjc.es/',
    'nombre' : 'Universidad Rey Juan Carlos',
  },
  "USAL"     : { 
    'web'    : 'http://usal.es/',
    'nombre' : 'Universidad de Salamanca',
  },
  "USC"      : { 
    'web'    : 'http://www.usc.es/',
    'nombre' : 'Universidade de Santiago de Compostela',
  },
  "US"       : { 
    'web'    : 'http://www.us.es/',
    'nombre' : 'Universidad de Sevilla',
  },
  "UNED"     : { 
    'web'    : 'https://www.uned.es/universidad/',
    'nombre' : 'Universidad Nacional de Educación a Distancia',
  },
  "UV"       : { 
    'web'    : 'https://www.uv.es/',
    'nombre' : 'Universitat de València',
  },
  "UVA"      : { 
    'web'    : 'http://www.uva.es',
    'nombre' : 'Universidad de Valladolid',
  },
  "UNIZAR"   : { 
    'web'    : 'http://unizar.es/',
    'nombre' : 'Universidad de Zaragoza',
  },
};

const l_provincias = {
  "A Coruña"           : {
    "codigo_provincia" : "C",
    "codigo_ccaa"      : "GA",
  },
  "Araba/Álava"        : {
    "codigo_provincia" : "VI​",
    "codigo_ccaa"      : "PV",
  },
  "Albacete"           : {
    "codigo_provincia" : "AB",
    "codigo_ccaa"      : "CM",
  },
  "Alicante/Alacant"   : {
    "codigo_provincia" : "A",
    "codigo_ccaa"      : "VC",
  },
  "Almería"            : {
    "codigo_provincia" : "AL",
    "codigo_ccaa"      : "AN",
  },
  "Asturias"           : {
    "codigo_provincia" : "O​",
    "codigo_ccaa"      : "AS​",
  },
  "Ávila"              : {
    "codigo_provincia" : "AV",
    "codigo_ccaa"      : "CL",
  },
  "Badajoz"            : {
    "codigo_provincia" : "BA",
    "codigo_ccaa"      : "EX",
  },
  "Balears"            : {
    "codigo_provincia" : "PM​",
    "codigo_ccaa"      : "IB",
  },
  "Barcelona"          : {
    "codigo_provincia" : "B",
    "codigo_ccaa"      : "CT",
  },
  "Bizkaia"            : {
    "codigo_provincia" : "BI​",
    "codigo_ccaa"      : "PV​",
  },
  "Burgos"             : {
    "codigo_provincia" : "BU",
    "codigo_ccaa"      : "CL",
  },
  "Cáceres"            : {
    "codigo_provincia" : "CC",
    "codigo_ccaa"      : "EX",
  },
  "Cádiz"              : {
    "codigo_provincia" : "CA",
    "codigo_ccaa"      : "AN",
  },
  "Cantabria"          : {
    "codigo_provincia" : "S​",
    "codigo_ccaa": "CB",
  },
  "Castellón/Castelló​" : {
    "codigo_provincia" : "CS",
    "codigo_ccaa"      : "VC",
  },
  "Ciudad Real"        : {
    "codigo_provincia" : "CR",
    "codigo_ccaa"      : "CM",
  },
  "Córdoba"            : {
    "codigo_provincia" : "CO",
    "codigo_ccaa"      : "AN",
  },
  "Cuenca"             : {
    "codigo_provincia" : "CU",
    "codigo_ccaa"      : "CM",
  },
  "Gipuzkoa​"           : {
    "codigo_provincia" : "SS​",
    "codigo_ccaa"      : "PV",
  },
  "Girona"             : {
    "codigo_provincia" : "GI",
    "codigo_ccaa"      : "CT",
  },
  "Granada"            : {
    "codigo_provincia" : "GR",
    "codigo_ccaa"      : "AN",
  },
  "Guadalajara"        : {
    "codigo_provincia" : "GU",
    "codigo_ccaa"      : "CM",
  },
  "Huelva"             : {
    "codigo_provincia" : "H",
    "codigo_ccaa"      : "AN",
  },
  "Huesca"             : {
    "codigo_provincia" : "HU",
    "codigo_ccaa"      : "AR",
  },
  "Jaén"               : {
    "codigo_provincia" : "J",
    "codigo_ccaa"      : "AN",
  },
  "La Rioja"           : {
    "codigo_provincia" : "LO​",
    "codigo_ccaa"      : "RI",
  },
  "Las Palmas"         : {
    "codigo_provincia" : "GC​",
    "codigo_ccaa"      : "CN​",
  },
  "León"               : {
    "codigo_provincia" : "LE",
    "codigo_ccaa"      : "CL",
  },
  "Lleida"             : {
    "codigo_provincia" : "L",
    "codigo_ccaa"      : "CT",
  },
  "Lugo"               : {
    "codigo_provincia" : "LU",
    "codigo_ccaa"      : "GA",
  },
  "Madrid"             : {
    "codigo_provincia" : "M",
    "codigo_ccaa"      : "MD",
  },
  "Málaga"             : {
    "codigo_provincia" : "MA",
    "codigo_ccaa"      : "AN",
  },
  "Murcia"             : {
    "codigo_provincia" : "MU",
    "codigo_ccaa"      : "MC",
  },
  "Navarra/Nafarroa​"   : {
    "codigo_provincia" : "NA",
    "codigo_ccaa"      : "NC",
  },
  "Ourense"            : {
    "codigo_provincia" : "OR",
    "codigo_ccaa"      : "GA",
  },
  "Palencia"           : {
    "codigo_provincia" : "P",
    "codigo_ccaa"      : "CL",
  },
  "Pontevedra"         : {
    "codigo_provincia" : "PO",
    "codigo_ccaa"      : "GA",
  },
  "Salamanca"          : {
    "codigo_provincia" : "SA",
    "codigo_ccaa"      : "CL",
  },
  "Santa Cruz de Tenerife" : {
    "codigo_provincia" : "TF",
    "codigo_ccaa"      : "CN",
  },
  "Segovia"            : {
    "codigo_provincia" : "SG",
    "codigo_ccaa"      : "CL",
  },
  "Sevilla"            : {
    "codigo_provincia" : "SE",
    "codigo_ccaa"      : "AN",
  },
  "Soria"              : {
    "codigo_provincia" : "SO",
    "codigo_ccaa"      : "CL",
  },
  "Tarragona"          : {
    "codigo_provincia" : "T",
    "codigo_ccaa"      : "CT",
  },
  "Teruel"             : {
    "codigo_provincia" : "TE",
    "codigo_ccaa"      : "AR",
  },
  "Toledo"             : {
    "codigo_provincia" : "TO",
    "codigo_ccaa"      : "CM",
  },
  "Valencia/València"  : {
    "codigo_provincia" : "V",
    "codigo_ccaa"      : "VC",
  },
  "Valladolid"         : {
    "codigo_provincia" : "VA",
    "codigo_ccaa"      : "CL",
  },
  "Zamora"             : {
    "codigo_provincia" : "ZA",
    "codigo_ccaa"      : "CL",
  },
  "Zaragoza"           : {
    "codigo_provincia" : "Z",
    "codigo_ccaa"      : "AR",
  },
};

const l_idiomas = {
  "es": 'español',
  "en": 'inglés',
  "cat": 'catalán',
};

const l_estudios = {
  "mat": {
    "nombre": 'Matemáticas fundamentales',
    "color" : '#c42021',
  },
  "apl": {
    "nombre": 'Matemáticas aplicadas',
    "color" : '#f038ff',
  },
  "est": {
    "nombre": 'Estadística',
    "color" : '#ff5714',
  },
  "dat": {
    "nombre": 'Ciencia de datos',
    "color" : '#7e1946',
  },
  "com": {
    "nombre": 'Computación',
    "color" : '#02c39a',
  },
  "fis": {
    "nombre": 'Física',
    "color" : '#3c887e',
  },
  "inf": {
    "nombre": 'Informática',
    "color" : '#3772ff',
  },
  "eco": {
    "nombre": 'Economía',
    "color" : '#e2ef70',
  },
  "edu": {
    "nombre": 'Educación',
    "color" : '#70e4ef',
  },
  "soc": {
    "nombre": 'Sociología',
    "color" : '#e0f2e9',
  },
};

function crear_mapa() {
  document.getElementById('map-warning').outerHTML = "";

  mapa_de_grados = L.map('mapid').setView([36.671234, -7.639734], 5);

  L.tileLayer(
    'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', 
    {
      attribution: '&copy; <a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
      maxZoom: 19,
    }
  ).addTo(mapa_de_grados);

  function texto_Popup(entrada) {
    if (favoritos_ver().includes(entrada.identificador)) {
      boton_favorito = '<button id="fav-' + entrada.identificador + '" aria-label="Eliminar de favoritos" class="button is-danger is-small is-pulled-right"  onclick="favoritos_actualizar_boton(\'' + entrada.identificador + '\')"><span class="icon"><i class="fas fa-ban"></i></span></button>';
    } else {
      boton_favorito = '<button id="fav-' + entrada.identificador + '" aria-label="Marcar como favorito"  class="button is-success is-small is-pulled-right" onclick="favoritos_actualizar_boton(\'' + entrada.identificador + '\')"><span class="icon"><i class="fas fa-star"></i></span></button>';
    };

    text  = '';
    text += '<div class="content">';
    text += '<h3 class="title is-5"><span><a href="../?identificador=' + entrada.identificador + '">' + entrada.nombre + '</a></span></h3>';
    text += '<ul style="margin-left:0;list-style:none;"><li><span class="icon"><i class="fas fa-university"></i></span>&nbsp;<span><a href="../?universidad=' + entrada.universidad + '">' + l_universidades[entrada.universidad].nombre + '</a></span></li>';
    text += '<li><span class="icon"><i class="far fa-calendar-alt"></i></span>&nbsp;' + entrada.creditos + ' créditos durante ' + entrada.anos + ' años</li>';
    text += '<li><span class="icon"><i class="far fa-money-bill-alt"></i></span>&nbsp;' + entrada.precio.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1&nbsp;") + '&nbsp;€ por año</li></ul>';
    text += '<div class="field is-grouped is-grouped-multiline">';
    for (var i = 0; i < entrada.estudios.length; i++) {
      text += '<div class="control"><div class="tags has-addons">';
      text += '<span class="tag is-light">' + l_estudios[entrada.estudios[i]].nombre + '</span>';
      text += '<span class="tag" style="background-color:' + l_estudios[entrada.estudios[i]].color + '!important"></span>';
      text += '</div></div>';
    };
    text += '</div>';
    text += boton_favorito;
    text += '</div><br>';

    return text;
  }

  function markerHtmlStyles(color) {return `
    background-color: ${color};
    border: 2px solid #FFFFFF;
    border-radius: 3rem 3rem 0;
    box-shadow: 4px 4px 3px rgba(30,30,30,0.7);
    display: block;
    height: 1.6rem;
    left: -0.8rem;
    position: relative;
    top: -0.8rem;
    transform: rotate(45deg);
    width: 1.6rem;`
  }

  function icono(color) {
    return L.divIcon({
      className: "icono-grados",
      iconAnchor: [0, 24],
      labelAnchor: [-6, 0],
      popupAnchor: [0, -36],
      html: `<span style="${markerHtmlStyles(color)}" />`
    })
  }

  var marcadores = L.markerClusterGroup({
    'disableClusteringAtZoom': 15,
    'showCoverageOnHover': false,
    'spiderfyOnMaxZoom': false,
  });

  function nuevo_marcador(entrada) {
    marcadores.addLayer(L.marker(entrada.coordenadas, {icon: icono(l_estudios[entrada.estudios[0]].color)}).bindPopup(texto_Popup(entrada)));
  }

  for (var i = datos.length - 1; i >= 0; i--) {
    nuevo_marcador(datos[i]);
  }

  mapa_de_grados.addLayer(marcadores);
  marcadores.refreshClusters();

};

function format(d) {

  var es_fav = favoritos_ver().includes(d.identificador);

  return `
    <div class="content">
      <h2 class="title is-4" style="margin-top:18px">
        ${d.nombre}
        <div class="buttons" style="float:right;">
          <a aria-label="Imprimir" class="button is-small is-light sin-sombra" href="./imprimir/?identificador=${d.identificador}">
            <span class="icon">
              <i class="fas fa-print"></i>
            </span>
          </a>
          <button id="fav-${d.identificador}" aria-label="${es_fav ? 'Eliminar de favoritos' : 'Marcar como favorito'}"  class="button is-small is-${es_fav ? 'danger' : 'success'}" onclick="favoritos_actualizar_boton('${d.identificador}')">
            <span class="icon">
              <i class="fas fa-${es_fav ? 'ban' : 'star'}"></i>
            </span>
          </button>
        </div>
      </h2>

      <div class="columns">

        <div class="column">
          <p>${d.info}</p>
        </div>

        <div class="column">

          <div class="field is-grouped is-grouped-multiline">${d.estudios.map(x => `
            <div class="control">
              <div class="tags has-addons">
                <span class="tag is-light">${l_estudios[x].nombre}</span>
                <span class="tag" style="background-color:${l_estudios[x].color}!important"></span>
              </div>
            </div>`).join('')}
          </div>

          <p><span class="icon"><i class="fas fa-user-graduate"></i></span><b>${d.tipo}</b></p>
          <p><span class="icon"><i class="fas fa-university"></i></span><b>Universidad:</b> <a href="${l_universidades[d.universidad].web}">${l_universidades[d.universidad].nombre}</a></p>
          <p><span class="icon"><i class="fas fa-map-marker-alt"></i></span><b>Impartido en:</b> ${(d.lugar === d.provincia) ? `${d.lugar}</p>` : `${d.lugar} (${d.provincia})</p>`}
          <p><span class="icon"><i class="fas fa-pen-nib"></i></span><b>Idiomas:</b> ${d.idiomas.map(e => l_idiomas[e]).join(", ")}</p>
          <p><span class="icon"><i class="fas fa-clipboard-list"></i></span><b>Optatividad:</b> ${d.optativas_creds} créditos, a elegir entre ${d.optativas_num} asignaturas.</p>
          ${!(d.menciones[0] == "") ? `
            <p style="margin-bottom:0.5em;">
              <span class="icon">
                <i class="fas fa-medal"></i>
              </span><b>Menciones:</b>
            </p>
            <ul style="margin-top:0;">
              <li>${d.menciones.join("</li><li>")}</li>
            </ul>` : ''}
          <p style="margin-bottom:0.5em;"><span class="icon"><i class="fas fa-link"></i></span><b>Enlaces de interés:</b></p>
          <ul style="margin-top:0;">
            ${Object.keys(d.enlaces).map(e=>`<li><p><a href="${d.enlaces[e]}">${e}</a></p></li>`).join('')}
          </ul>
        </div>

      </div>
      
      <p><b>Estadísticas de ingreso:</b></p>
      <canvas id="${d.identificador}" class="acceso" width="400" height="75"></canvas>
      
    </div>`;
};

function crear_tabla () {

  var datos_tabla = filtrar(datos, getQueryParams(document.location.search));

  var table = $("#grados").DataTable( {
      "bAutoWidth": false,
      "data": datos_tabla,

      "columns": [
        {
          "className": 'details-control',
          "orderable": false,
          "data": null,
          "defaultContent": ''
        },
        { 
          "data": "nombre",
          "title": "Nombre",
          "render": 
            function(data, type, row, meta) {
              data = '<a href="' + row.enlace + '">' + data + '</a>';
              return data;
            },
            "width": "50%",
        },
        { 
          "data": "universidad",
          "title": "Universidad",
          "render":
            function(data, type, row, meta) {
              data = '<a href="' + l_universidades[data].web + '">' + data + '</a>';
              return data;
            },
          "width": "12%",
        },
        { 
          "data": "provincia",
          "title": "Provincia",
          "width": "12%",
        },
        { 
          "data": "creditos",
          "title": "ECTS",
          "type": "num",
          "visible": false,
        },
        { 
          "data": "creditos",
          "title": "ECTS",
          "type": "num",
          "className": 'dt-body-right',
          "render": 
            function(data, type, row, meta) {
              data = row.creditos_por_ano + '&nbsp;×&nbsp;' + row.anos + '&nbsp;=&nbsp;' + data;
              return data;
            },
          "orderData": [4],
          "width": "12%",
        },
        { 
          "data": "precio",
          "render": $.fn.dataTable.render.number('&nbsp;', '.', 2, '', '&nbsp;€'),
          "title": "Precio",
          "type": "num-fmt",
          "className": 'dt-body-right',
          "width": "12%",
        },
      ],

      "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ entradas",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando entradas de la _START_ a la _END_ de un total de _TOTAL_ entradas",
        "sInfoEmpty":     "No hay entradas para mostrar",
        "sInfoFiltered":  "(filtradas de un total de _MAX_ entradas)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands": ",",
        "sLoadingRecords":"Cargando...",
        "oPaginate": {
          "sFirst":     "Primera",
          "sLast":      "Última",
          "sNext":      "Siguiente",
          "sPrevious":  "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
    }, 
  );

  table.order([1,"asc"]).draw();

  $('#grados tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    if (row.child.isShown()) {
      row.child.hide();
      tr.removeClass('shown');
    } else {
      $('#' + row.data().identificador).remove();
      row.child(format(row.data())).show();
      tr.addClass('shown');
      if (Object.keys(row.data()).indexOf("datos_acceso") > -1) {
        
        Chart.defaults.global.defaultFontFamily = '"Source Sans Pro", "Helvetica", "Arial", sans-serif';

        var grafica_anos = JSON.parse(JSON.stringify(row.data().datos_acceso.curso));
        var grafica_nota = JSON.parse(JSON.stringify(row.data().datos_acceso.nota_de_corte));
        var grafica_plazas = JSON.parse(JSON.stringify(row.data().datos_acceso.plazas));

        var datos_estadisticas = {
          labels: grafica_anos.reverse(),
          datasets: [{
            type: 'line',
            label: 'Nota de corte',
            borderWidth: 3,
            borderColor: '#0D2481',
            pointRadius: 8,
            pointHitRadius: 18,
            backgroundColor: '#0D2481',
            fill: false,
            data: grafica_nota.reverse(),
            yAxisID: 'eje-nota',
          }, {
            type: 'bar',
            label: 'Plazas de nuevo ingreso',
            borderColor: '#FFD6D1',
            backgroundColor: '#FFD6D1',
            data: grafica_plazas.reverse(),
            yAxisID: 'eje-plazas',
          }]
        };

        var ctx = document.getElementById(row.data().identificador).getContext('2d');
        window.myMixedChart = new Chart(ctx, {
          type: 'bar',
          data: datos_estadisticas,
          options: {
            responsive: true,
            tooltips: {
              mode: 'nearest',
              intersect: true
            },
            scales: {
              yAxes: [{
                type: 'linear',
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 14,
                },
                display: true,
                position: 'left',
                id: 'eje-nota',
              }, {
                type: 'linear',
                ticks: {
                  beginAtZero: true,
                },
                display: true,
                position: 'right',
                id: 'eje-plazas',
                gridLines: {
                  drawOnChartArea: false,
                },
              }],
            },
          }
        });
      }
    }
  });

  return table
};



function buscar() {
  b = [];

  us = $('#select-uni').multipleSelect('getSelects').join(',');
  if (!(us == "")) {b.push('universidad=' + us)};

  es = $('#select-cat').multipleSelect('getSelects').join(',');
  if (!(es == "")) {b.push('estudios=' + es)};

  ps = $('#select-prov').multipleSelect('getSelects').join(',');
  if (!(ps == "")) {b.push('provincia=' + ps)};

  cs = $('#select-ccaa').multipleSelect('getSelects').join(',');
  if (!(cs == "")) {b.push('ccaa=' + cs)};

  if (!(b == [])) {b = '?' + b.join('&')}

  window.history.pushState({}, 'Buscar','./'+b);

  table.clear().rows.add(filtrar(datos, getQueryParams(document.location.search))).draw()
};

function mounted(nombre) {
  $('#select-fake-' + nombre).remove();
  $('#select-' + nombre).multipleSelect({
    filter: true
  });
  $(document).on("click", function () {
    if ($("#select-" + nombre + "-parent .ms-drop").css("display") == "block") {
      $("#select-" + nombre + "-parent .ms-choice").addClass("ms-choice-active");
    } else {
      $("#select-" + nombre + "-parent .ms-choice").removeClass("ms-choice-active");
    }
  });
}

function randomId() {
  return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}

function filtrar(datos, filtros) {
  if (Object.keys(filtros).length === 0) {
    return datos;
  } else {
    datos_filtrados = [];

    for (categoria in filtros) {
      filtros[categoria] = filtros[categoria].split(",").map(function(item) {return item.trim();});
    }


    for (i = 0; i < datos.length; i++) {
      for (categoria in filtros) {
        var dato;
        switch(categoria) {
          case 'ccaa': 
            dato = l_provincias[datos[i]['provincia']].codigo_ccaa;
            break
          case 'provincia': 
            dato = l_provincias[datos[i]['provincia']].codigo_provincia;
            break
          default: 
            dato = datos[i][categoria];
            break
        } 
        if (dato.constructor === Array) {
          if (intersect(dato, filtros[categoria]).length > 0) {
            datos_filtrados.push(datos[i]);
            break;
          }
        } else {
          if (filtros[categoria].includes(dato)) {
            datos_filtrados.push(datos[i]);
            break;
          }          
        }
      }
    }
    return datos_filtrados;
  }
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

var cookie_nombre = 'anem_grados';
var cookie_dias   = 28;

function favoritos_guardar(identificador) {
  var cookie = getCookie(cookie_nombre);
  if (cookie == null) {
    setCookie(cookie_nombre,identificador,cookie_dias);
    return true;
  } else {
    ids = (cookie === "") ? [] : cookie.split(",");
    if (ids.includes(identificador)) {
      return false;
    } else {
      ids.push(identificador);
      setCookie(cookie_nombre,ids.toString(),cookie_dias);
      return true;
    };
  };
};

function favoritos_eliminar(identificador) {
  var cookie = getCookie(cookie_nombre);
  if (cookie == null) {
    return false;
  } else {
    ids = cookie.split(",");
    indice = ids.indexOf(identificador);
    if (indice > -1) {
      ids.splice(indice,1)
      setCookie(cookie_nombre,ids,cookie_dias);
      return true;
    } else {
      return false;
    };
  };
};

function favoritos_actualizar_boton(identificador) {
  if ($('#fav-' + identificador).hasClass('is-success')) {
    favoritos_guardar(identificador);
    $('#fav-' + identificador).addClass('is-danger').removeClass('is-success');
    $('#fav-' + identificador + ' i').removeClass('fa-star').addClass('fa-ban');
  } else {
    favoritos_eliminar(identificador);
    $('#fav-' + identificador).addClass('is-success').removeClass('is-danger');
    $('#fav-' + identificador + ' i').removeClass('fa-ban').addClass('fa-star');
  }
}

function favoritos_ver() {
  var cookies = getCookie(cookie_nombre);
  if (cookies == null) {
    return ['']
  } else {
    return cookies.split(",");
  }
};

function favoritos_verenlista() {
  window.history.pushState('favoritos', '', './?identificador=' + getCookie(cookie_nombre));
  table.clear().rows.add(filtrar(datos, getQueryParams(document.location.search))).draw();
}

function favoritos_vertablacompleta() {
  window.history.pushState('favoritos', '', './');
  table.clear().rows.add(datos).draw();
}

function favoritos_limpiar() {
  setCookie(cookie_nombre,[''],cookie_dias);
};

function intersect(a, b) {
  var t;
  if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
  return a.filter(function (e) {
    return b.indexOf(e) > -1;
  });
}

// multiple-select.js

/**
 * @author zhixin wen <wenzhixin2010@gmail.com>
 * @version 1.2.2
 *
 * http://wenzhixin.net.cn/p/multiple-select/
 */

(function ($) {

    'use strict';

    // it only does '%s', and return '' when arguments are undefined
    var sprintf = function (str) {
        var args = arguments,
            flag = true,
            i = 1;

        str = str.replace(/%s/g, function () {
            var arg = args[i++];

            if (typeof arg === 'undefined') {
                flag = false;
                return '';
            }
            return arg;
        });
        return flag ? str : '';
    };

    var removeDiacritics = function (str) {
        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
            {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
            {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
            {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
            {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
            {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
            {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
            {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
            {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
        ];

        for (var i = 0; i < defaultDiacriticsRemovalMap.length; i++) {
            str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
        }

        return str;

   };

    function MultipleSelect($el, options) {
        var that = this,
            name = $el.attr('name') || options.name || '';

        this.options = options;

        // hide select element
        this.$el = $el.hide();

        // label element
        this.$label = this.$el.closest('label');
        if (this.$label.length === 0 && this.$el.attr('id')) {
            this.$label = $(sprintf('label[for="%s"]', this.$el.attr('id').replace(/:/g, '\\:')));
        }

        // restore class and title from select element
        this.$parent = $(sprintf(
            '<div class="ms-parent %s" %s/>',
            $el.attr('class') || '',
            sprintf('title="%s"', $el.attr('title'))));

        // add placeholder to choice button
        this.$choice = $(sprintf([
                '<button type="button" class="ms-choice">',
                '<span class="placeholder">%s</span>',
                '<div></div>',
                '</button>'
            ].join(''),
            this.options.placeholder));

        // default position is bottom
        this.$drop = $(sprintf('<div class="ms-drop %s"%s></div>',
            this.options.position,
            sprintf(' style="width: %s"', this.options.dropWidth)));

        this.$el.after(this.$parent);
        this.$parent.append(this.$choice);
        this.$parent.append(this.$drop);

        if (this.$el.prop('disabled')) {
            this.$choice.addClass('disabled');
        }
        this.$parent.css('width',
            this.options.width ||
            this.$el.css('width') ||
            this.$el.outerWidth() + 20);

        this.selectAllName = 'data-name="selectAll' + name + '"';
        this.selectGroupName = 'data-name="selectGroup' + name + '"';
        this.selectItemName = 'data-name="selectItem' + name + '"';

        if (!this.options.keepOpen) {
            $(document).click(function (e) {
                if ($(e.target)[0] === that.$choice[0] ||
                    $(e.target).parents('.ms-choice')[0] === that.$choice[0]) {
                    return;
                }
                if (($(e.target)[0] === that.$drop[0] ||
                    $(e.target).parents('.ms-drop')[0] !== that.$drop[0] && e.target !== $el[0]) &&
                    that.options.isOpen) {
                    that.close();
                }
            });
        }
    }

    MultipleSelect.prototype = {
        constructor: MultipleSelect,

        init: function () {
            var that = this,
                $ul = $('<ul></ul>');

            this.$drop.html('');

            if (this.options.filter) {
                this.$drop.append([
                    '<div class="ms-search">',
                    '<div class="field">',
                    '<p class="control has-icons-left">',
                    '<input class="input" type="text" autocomplete="off" autocorrect="off" autocapitilize="off" spellcheck="false" placeholder="Buscar...">',
                    '<span class="icon is-small is-left"><i class="fas fa-search"></i></span>',
                    '</p>',
                    '</div>',
                    '</div>'].join('')
                );
            }

            var sa_random_id = 'input_selectAll_' + randomId() + randomId() + randomId();

            if (this.options.selectAll && !this.options.single) {
                $ul.append([
                    '<li class="ms-select-all">',
                    '<div class="field">',
                    '<p class="control">',
                    '<div class="b-checkbox is-primary is-inline">',
                    sprintf('<input id="%s" type="checkbox" class="styled" type="checkbox" %s >', sa_random_id, this.selectAllName),
                    sprintf('<label for="%s">', sa_random_id),
                    this.options.formatSelectAll(),
                    '</label>',
                    '</div>',
                    '</p>',
                    '</div>',
                    '</li>'
                ].join(''));
            }

            $.each(this.$el.children(), function (i, elm) {
                $ul.append(that.optionToHtml(i, elm));
            });
            $ul.append(sprintf('<li class="ms-no-results">%s</li>', this.options.formatNoMatchesFound()));
            this.$drop.append($ul);

            this.$drop.find('ul').css('max-height', this.options.maxHeight + 'px');
            this.$drop.find('.multiple').css('width', this.options.multipleWidth + 'px');

            this.$searchInput = this.$drop.find('.ms-search input');
            this.$selectAll = this.$drop.find('input[' + this.selectAllName + ']');
            this.$selectGroups = this.$drop.find('input[' + this.selectGroupName + ']');
            this.$selectItems = this.$drop.find('input[' + this.selectItemName + ']:enabled');
            this.$disableItems = this.$drop.find('input[' + this.selectItemName + ']:disabled');
            this.$noResults = this.$drop.find('.ms-no-results');

            this.events();
            this.updateSelectAll(true);
            this.update(true);

            if (this.options.isOpen) {
                this.open();
            }

            if (this.options.openOnHover) {
                $('.ms-parent').hover(function (e) {
                    that.open();
                });
            }
        },

        optionToHtml: function (i, elm, group, groupDisabled) {
            var that = this,
                $elm = $(elm),
                classes = $elm.attr('class') || '',
                title = sprintf('title="%s"', $elm.attr('title')),
                multiple = this.options.multiple ? 'multiple' : '',
                disabled,
                type = this.options.single ? 'radio' : 'checkbox';

            if ($elm.is('option')) {
                var value = $elm.val(),
                    text = that.options.textTemplate($elm),
                    selected = $elm.prop('selected'),
                    style = sprintf('style="%s"', this.options.styler(value)),
                    $el;

                disabled = groupDisabled || $elm.prop('disabled');

                $el = $([
                    sprintf('<li class="%s %s" %s %s>', multiple, classes, title, style),
                    '<div class="field">',
                    '<p class="control">',
                    '<div class="b-checkbox is-primary is-inline">',
                    sprintf('<input type="%s" id="%s" class="styled" %s%s%s%s>',
                        type, value, this.selectItemName,
                        selected ? ' checked="checked"' : '',
                        disabled ? ' disabled="disabled"' : '',
                        sprintf(' data-group="%s"', group)),
                    sprintf('<label for="%s" class="%s">', value, disabled ? 'disabled' : ''),
                    sprintf('<span>%s</span>', text),
                    '</label>',
                    '</div>',
                    '</p>',
                    '</div>',
                    '</li>'
                ].join(''));
                $el.find('input').val(value);
                return $el;
            }
            if ($elm.is('optgroup')) {
                var label = that.options.labelTemplate($elm),
                    $group = $('<div/>');

                group = 'group_' + i;
                disabled = $elm.prop('disabled');

                $group.append([
                    '<li class="group">',
                    sprintf('<label class="optgroup %s" data-group="%s">', disabled ? 'disabled' : '', group),
                    this.options.hideOptgroupCheckboxes || this.options.single ? '' :
                        sprintf('<input type="checkbox" %s %s>',
                        this.selectGroupName, disabled ? 'disabled="disabled"' : ''),
                    label,
                    '</label>',
                    '</li>'
                ].join(''));

                $.each($elm.children(), function (i, elm) {
                    $group.append(that.optionToHtml(i, elm, group, disabled));
                });
                return $group.html();
            }
        },

        events: function () {
            var that = this,
                toggleOpen = function (e) {
                    e.preventDefault();
                    that[that.options.isOpen ? 'close' : 'open']();
                };

            if (this.$label) {
                this.$label.off('click').on('click', function (e) {
                    if (e.target.nodeName.toLowerCase() !== 'label' || e.target !== this) {
                        return;
                    }
                    toggleOpen(e);
                    if (!that.options.filter || !that.options.isOpen) {
                        that.focus();
                    }
                    e.stopPropagation(); // Causes lost focus otherwise
                });
            }

            this.$choice.off('click').on('click', toggleOpen)
                .off('focus').on('focus', this.options.onFocus)
                .off('blur').on('blur', this.options.onBlur);

            this.$parent.off('keydown').on('keydown', function (e) {
                switch (e.which) {
                    case 27: // esc key
                        that.close();
                        that.$choice.focus();
                        break;
                }
            });

            this.$searchInput.off('keydown').on('keydown',function (e) {
                // Ensure shift-tab causes lost focus from filter as with clicking away
                if (e.keyCode === 9 && e.shiftKey) {
                    that.close();
                }
            }).off('keyup').on('keyup', function (e) {
                // enter or space
                // Avoid selecting/deselecting if no choices made
                if (that.options.filterAcceptOnEnter && (e.which === 13 || e.which == 32) && that.$searchInput.val()) {
                    that.$selectAll.click();
                    that.close();
                    that.focus();
                    return;
                }
                that.filter();
            });

            this.$selectAll.off('click').on('click', function () {
                var checked = $(this).prop('checked'),
                    $items = that.$selectItems.filter(':visible');

                if ($items.length === that.$selectItems.length) {
                    that[checked ? 'checkAll' : 'uncheckAll']();
                } else { // when the filter option is true
                    that.$selectGroups.prop('checked', checked);
                    $items.prop('checked', checked);
                    that.options[checked ? 'onCheckAll' : 'onUncheckAll']();
                    that.update();
                }
            });
            this.$selectGroups.off('click').on('click', function () {
                var group = $(this).parent().attr('data-group'),
                    $items = that.$selectItems.filter(':visible'),
                    $children = $items.filter(sprintf('[data-group="%s"]', group)),
                    checked = $children.length !== $children.filter(':checked').length;

                $children.prop('checked', checked);
                that.updateSelectAll();
                that.update();
                that.options.onOptgroupClick({
                    label: $(this).parent().text(),
                    checked: checked,
                    children: $children.get(),
                    instance: that
                });
            });
            this.$selectItems.off('click').on('click', function () {
                that.updateSelectAll();
                that.update();
                that.updateOptGroupSelect();
                that.options.onClick({
                    label: $(this).parent().text(),
                    value: $(this).val(),
                    checked: $(this).prop('checked'),
                    instance: that
                });

                if (that.options.single && that.options.isOpen && !that.options.keepOpen) {
                    that.close();
                }

                if (that.options.single) {
                    var clickedVal = $(this).val();
                    that.$selectItems.filter(function() {
                        return $(this).val() !== clickedVal;
                    }).each(function() {
                        $(this).prop('checked', false);
                    });
                    that.update();
                }
            });
        },

        open: function () {
            if (this.$choice.hasClass('disabled')) {
                return;
            }
            this.options.isOpen = true;
            this.$choice.find('>div').addClass('open');
            this.$drop[this.animateMethod('show')]();

            // fix filter bug: no results show
            this.$selectAll.parent().show();
            this.$noResults.hide();

            // Fix #77: 'All selected' when no options
            if (!this.$el.children().length) {
                this.$selectAll.parent().hide();
                this.$noResults.show();
            }

            if (this.options.container) {
                var offset = this.$drop.offset();
                this.$drop.appendTo($(this.options.container));
                this.$drop.offset({
                    top: offset.top,
                    left: offset.left
                });
            }

            if (this.options.filter) {
                this.$searchInput.val('');
                this.$searchInput.focus();
                this.filter();
            }
            this.options.onOpen();
        },

        close: function () {
            this.options.isOpen = false;
            this.$choice.find('>div').removeClass('open');
            this.$drop[this.animateMethod('hide')]();
            if (this.options.container) {
                this.$parent.append(this.$drop);
                this.$drop.css({
                    'top': 'auto',
                    'left': 'auto'
                });
            }
            this.options.onClose();
        },

        animateMethod: function (method) {
            var methods = {
                show: {
                    fade: 'fadeIn',
                    slide: 'slideDown'
                },
                hide: {
                    fade: 'fadeOut',
                    slide: 'slideUp'
                }
            };

            return methods[method][this.options.animate] || method;
        },

        update: function (isInit) {
            var selects = this.options.displayValues ? this.getSelects() : this.getSelects('text'),
                $span = this.$choice.find('>span'),
                sl = selects.length;

            if (sl === 0) {
                $span.addClass('placeholder').html(this.options.placeholder);
            } else if (this.options.formatAllSelected() && sl === this.$selectItems.length + this.$disableItems.length) {
                $span.removeClass('placeholder').html(this.options.formatAllSelected);
            } else if (this.options.ellipsis && sl > this.options.minimumCountSelected) {
                $span.removeClass('placeholder').text(selects.slice(0, this.options.minimumCountSelected)
                    .join(this.options.delimiter) + '...');
            } else if (this.options.formatCountSelected() && sl > this.options.minimumCountSelected) {
                $span.removeClass('placeholder').html(this.options.formatCountSelected()
                    .replace('#', selects.length)
                    .replace('%', this.$selectItems.length + this.$disableItems.length));
            } else {
                $span.removeClass('placeholder').text(selects.join(this.options.delimiter));
            }

            if (this.options.addTitle) {
                $span.prop('title', this.getSelects('text'));
            }

            // set selects to select
            this.$el.val(this.getSelects()).trigger('change');

            // add selected class to selected li
            this.$drop.find('li').removeClass('selected');
            this.$drop.find('input:checked').each(function () {
                $(this).parents('li').first().addClass('selected');
            });

            // trigger <select> change event
            if (!isInit) {
                this.$el.trigger('change');
            }
        },

        updateSelectAll: function (isInit) {
            var $items = this.$selectItems;

            if (!isInit) {
                $items = $items.filter(':visible');
            }
            this.$selectAll.prop('checked', $items.length &&
                $items.length === $items.filter(':checked').length);
            if (!isInit && this.$selectAll.prop('checked')) {
                this.options.onCheckAll();
            }
        },

        updateOptGroupSelect: function () {
            var $items = this.$selectItems.filter(':visible');
            $.each(this.$selectGroups, function (i, val) {
                var group = $(val).parent().attr('data-group'),
                    $children = $items.filter(sprintf('[data-group="%s"]', group));
                $(val).prop('checked', $children.length &&
                    $children.length === $children.filter(':checked').length);
            });
        },

        //value or text, default: 'value'
        getSelects: function (type) {
            var that = this,
                texts = [],
                values = [];
            this.$drop.find(sprintf('input[%s]:checked', this.selectItemName)).each(function () {
                texts.push($(this).parents('li').first().text());
                values.push($(this).val());
            });

            if (type === 'text' && this.$selectGroups.length) {
                texts = [];
                this.$selectGroups.each(function () {
                    var html = [],
                        text = $.trim($(this).parent().text()),
                        group = $(this).parent().data('group'),
                        $children = that.$drop.find(sprintf('[%s][data-group="%s"]', that.selectItemName, group)),
                        $selected = $children.filter(':checked');

                    if (!$selected.length) {
                        return;
                    }

                    html.push('[');
                    html.push(text);
                    if ($children.length > $selected.length) {
                        var list = [];
                        $selected.each(function () {
                            list.push($(this).parent().text());
                        });
                        html.push(': ' + list.join(',&nbsp;'));
                    }
                    html.push(']');
                    texts.push(html.join(''));
                });
            }
            return type === 'text' ? texts : values;
        },

        setSelects: function (values) {
            var that = this;
            this.$selectItems.prop('checked', false);
            this.$disableItems.prop('checked', false);
            $.each(values, function (i, value) {
                that.$selectItems.filter(sprintf('[value="%s"]', value)).prop('checked', true);
                that.$disableItems.filter(sprintf('[value="%s"]', value)).prop('checked', true);
            });
            this.$selectAll.prop('checked', this.$selectItems.length ===
                this.$selectItems.filter(':checked').length + this.$disableItems.filter(':checked').length);

            $.each(that.$selectGroups, function (i, val) {
                var group = $(val).parent().attr('data-group'),
                    $children = that.$selectItems.filter('[data-group="' + group + '"]');
                $(val).prop('checked', $children.length &&
                    $children.length === $children.filter(':checked').length);
            });

            this.update();
        },

        enable: function () {
            this.$choice.removeClass('disabled');
        },

        disable: function () {
            this.$choice.addClass('disabled');
        },

        checkAll: function () {
            this.$selectItems.prop('checked', true);
            this.$selectGroups.prop('checked', true);
            this.$selectAll.prop('checked', true);
            this.update();
            this.options.onCheckAll();
        },

        uncheckAll: function () {
            this.$selectItems.prop('checked', false);
            this.$selectGroups.prop('checked', false);
            this.$selectAll.prop('checked', false);
            this.update();
            this.options.onUncheckAll();
        },

        focus: function () {
            this.$choice.focus();
            this.options.onFocus();
        },

        blur: function () {
            this.$choice.blur();
            this.options.onBlur();
        },

        refresh: function () {
            this.init();
        },

        destroy: function () {
            this.$el.show();
            this.$parent.remove();
            this.$el.data('multipleSelect', null);
        },

        filter: function () {
            var that = this,
                text = $.trim(this.$searchInput.val()).toLowerCase();

            if (text.length === 0) {
                this.$selectAll.parent().parent().show();
                this.$selectItems.parent().parent().show();
                this.$selectItems.each(function () {
                  $(this).parent().parent().parent().css('display', 'list-item');
                });
                this.$disableItems.parent().parent().show();
                this.$selectGroups.parent().parent().show();
                this.$noResults.hide();
            } else {
                this.$selectItems.each(function () {
                    var $parent = $(this).parent().parent();
                    var $son = $(this).parent().children('label').children('span');
                    if (removeDiacritics($son.text().toLowerCase()).indexOf(removeDiacritics(text)) < 0) {
                      $parent['hide']();
                      $parent.parent().css('display', 'none');
                    } else {
                      $parent['show']();
                      $parent.parent().css('display', 'list-item');
                    }
                    
                });
                this.$disableItems.parent().hide();
                this.$selectGroups.each(function () {
                    var $parent = $(this).parent();
                    var group = $parent.attr('data-group'),
                        $items = that.$selectItems.filter(':visible');
                    $parent[$items.filter(sprintf('[data-group="%s"]', group)).length ? 'show' : 'hide']();
                });

                //Check if no matches found
                if (this.$selectItems.parent().filter(':visible').length) {
                    this.$selectAll.parent().show();
                    this.$noResults.hide();
                } else {
                    this.$selectAll.parent().hide();
                    this.$noResults.show();
                }
            }
            this.updateOptGroupSelect();
            this.updateSelectAll();
            this.options.onFilter(text);
        },

        destroy: function () {
            this.$el.before(this.$parent);
            this.$parent.remove();
            delete $.fn.multipleSelect;
        }
    };

    $.fn.multipleSelect = function () {
        var option = arguments[0],
            args = arguments,

            value,
            allowedMethods = [
                'getSelects', 'setSelects',
                'enable', 'disable',
                'open', 'close',
                'checkAll', 'uncheckAll',
                'focus', 'blur',
                'refresh', 'destroy'
            ];

        this.each(function () {
            var $this = $(this),
                data = $this.data('multipleSelect'),
                options = $.extend({}, $.fn.multipleSelect.defaults,
                    $this.data(), typeof option === 'object' && option);

            if (!data) {
                data = new MultipleSelect($this, options);
                $this.data('multipleSelect', data);
            }

            if (typeof option === 'string') {
                if ($.inArray(option, allowedMethods) < 0) {
                    throw 'Unknown method: ' + option;
                }
                value = data[option](args[1]);

                if (option === 'destroy') {
                    $this.removeData('multipleSelect');
                }
            } else {
                data.init();
                if (args[1]) {
                    value = data[args[1]].apply(data, [].slice.call(args, 2));
                }
            }
        });

        return typeof value !== 'undefined' ? value : this;
    };

    $.fn.multipleSelect.defaults = {
        name: '',
        placeholder: '',
        selectAll: true,
        allSelected: true,

        displayType: 'countSelected',
        displayValues: false,
        displayTitle: false,
        delimiter: ', ',
        minimumCountSelected: 3,
        ellipsis: false,

        single: false,
        multiple: false,
        multipleWidth: 80,
        hideOptgroupCheckboxes: false,
        width: undefined,
        dropWidth: undefined,
        maxHeight: 250,
        position: 'bottom',

        isOpen: false,
        keepOpen: false,
        openOnHover: false,

        filter: false,
        filterPlaceholder: '',
        filterAcceptOnEnter: false,
        container: null,
        animate: 'none',

        formatSelectAll: function () {
            return 'Seleccionar todas';
        },
        formatAllSelected: function () {
            return 'Todas seleccionadas';
        },
        formatCountSelected: function () {
            return '# de % seleccionadas';
        },
        formatNoMatchesFound: function () {
            return 'Sin coincidencias';
        },

        styler: function () {
            return false;
        },
        textTemplate: function ($elm) {
            return $elm.html();
        },
        labelTemplate: function ($elm) {
            return $elm.attr('label');
        },

        onOpen: function () {
            return false;
        },
        onClose: function () {
            return false;
        },
        onCheckAll: function () {
            return false;
        },
        onUncheckAll: function () {
            return false;
        },
        onFocus: function () {
            return false;
        },
        onBlur: function () {
            return false;
        },
        onOptgroupClick: function () {
            return false;
        },
        onClick: function () {
            return false;
        },
        onFilter: function () {
            return false;
        }
    };
})(jQuery);

// dataTables-bulma.js

(function(b) {
  "function" === typeof define && define.amd ? define(["jquery", "datatables.net"], function(a) {
    return b(a, window, document)
  }) : "object" === typeof exports ? module.exports = function(a, d) {
    a || (a = window);
    if (!d || !d.fn.dataTable) d = require("datatables.net")(a, d).$;
    return b(d, a, a.document)
  } : b(jQuery, window, document)
})(function(b, a, d) {
  var f = b.fn.dataTable;
  b.extend(!0, f.defaults, {
    dom: "<'columns'<'column is-6'l><'column is-6'f>><'columns'<'column is-12 table-container'tr>><'columns'<'column is-5'i><'column is-7'p>>",
    renderer: "bulma"
  });
  b.extend(f.ext.classes, {
    sWrapper: "dataTables_wrapper dt-bulma",
    sFilterInput: "input is-small",
    sLengthSelect: "input is-small",
    sProcessing: "dataTables_processing panel",
    sPageButton: "pagination-link",
    sPagePrevious: "pagination-previous",
    sPageNext: "pagination-next",
    sPageButtonActive: "is-current"
  });
  f.ext.renderer.pageButton.bulma = function(a, h, r, m, j, n) {
    var o = new f.Api(a),
      s = a.oClasses,
      k = a.oLanguage.oPaginate,
      t = a.oLanguage.oAria.paginate || {},
      e, g, p = 0,
      q = function(d, f) {
        var l, h, i, c, m = function(a) {
          a.preventDefault();
          (!b(a.currentTarget).is("[disabled]") && !b(a.currentTarget).is("#table_ellipsis")) && o.page() != a.data.action && o.page(a.data.action).draw("page")
        };
        l = 0;
        for (h = f.length; l < h; l++)
          if (c = f[l], b.isArray(c)) q(d, c);
          else {
            g = e = "";
            var dd = false;
            switch (c) {
              case "ellipsis":
                e = "&#x2026;";
                dd = true;
                break;
              case "first":
                e = k.sFirst;
                dd = c + (0 < j ? false : true);
                break;
              case "previous":
                e = k.sPrevious;
                dd = 0 < j ? false : true;
                break;
              case "next":
                e = k.sNext;
                dd = j < n - 1 ? false : true;
                break;
              case "last":
                e = k.sLast;
                dd = c + (j < n - 1 ? false : true);
                break;
              default:
                e = c + 1, g = j === c ? " is-current" : "";
                dd = false;
            }
            e && (i = b("<li>", {
              id: 0 === r && "string" === typeof c ? a.sTableId + "_" + c : null
            }).append(b("<a>", {
              "class": s.sPageButton + " " + g,
              href: "#",
              "aria-controls": a.sTableId,
              "aria-label": t[c],
              "data-dt-idx": p,
              tabindex: a.iTabIndex,
              disabled: dd
            }).html(e)).appendTo(d), a.oApi._fnBindAction(i, {
              action: c
            }, m), p++)
          }
      },
      i;
    try {
      i = b(h).find(d.activeElement).data("dt-idx")
    } catch (u) {}
    q(b(h).empty().html('<ul class="pagination-list"/>').children("ul"), m);
    i && b(h).find("[data-dt-idx=" + i + "]").focus()
  };
  return f
});
